apt-get update && apt-get upgrade -y

# ---------------------------------------
#          MongoDB Setup
# ---------------------------------------

# Importing the MongoDB public GPG Key
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -

# Downloading required libraries
sudo apt-get install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -

# Creating a list file for MongoDB
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

# Updating and installing packages
sudo apt-get update
sudo apt-get install -y mongodb-org